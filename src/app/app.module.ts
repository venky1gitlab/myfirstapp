import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgReduxModule, NgRedux } from '@angular-redux/store';
import {FormsModule} from '@angular/forms';
import {
  applyMiddleware,
  Store,
  combineReducers,
  compose,
  createStore,
} from 'redux';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {IAppState,rootReducer,INITIAL_STATE} from './store';
import { TodoOverviewComponent } from './todo-overview/todo-overview.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { CalculatorComponent } from './calculator/calculator.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoOverviewComponent,
    TodoListComponent,
    CalculatorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgReduxModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  // constructor(ngRedux : NgRedux<IAppState>) {
  //   ngRedux.provideStore(store);
  // }
}

// export const store: Store<IAppState> = createStore(
//   rootReducer
// );
